﻿using System;
using System.Windows;
using Backend;
using Backend.ViewModels;

namespace WPFProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private UserViewModel uvm = new UserViewModel(new UserRepository());
        public MainWindow()
        {
            InitializeComponent();
            DataContext = uvm;
        }

        private void Button_Click_Login(object sender, RoutedEventArgs e)
        {
            try
            {
                User user = uvm.LoginUser(this.inputUsername.Text, this.inputPassword.Password);
                MessageBox.Show(String.Format("Du er nu logget ind som {0}!", user.Username));
            }
            catch (UnauthorizedAccessException err)
            {
                MessageBox.Show(err.Message);
            }
        }
    }
}
