﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Backend.ViewModels
{
    public class UserViewModel
    {
        private Auth auth;
        public UserViewModel(UserRepository userRepo)
        {
            this.auth = new Auth(userRepo);
        }

        // Returns a the user on successful login. Throws UnauthorizedException on unsuccessful login.
        public User LoginUser(string username, string password)
        {
            try
            {
                User loggedInUser = this.auth.Login(username, password);
                return loggedInUser;
            }
            catch (UnauthorizedAccessException e)
            {
                throw e;
            }
        }
    }
}
