﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Backend
{
    public class Auth
    {
        public UserRepository UserRepo { get; set; }

        public Auth(UserRepository userRepo)
        {
            this.UserRepo = userRepo;
        }

        public User Login(string username, string password)
        {
            User user = UserRepo.GetByUsername(username);

            if (user == null)
                throw new UnauthorizedAccessException(string.Format("Der findes ikke en bruger, med brugernavnet {0}", username));

            if (user.Password != password)
                throw new UnauthorizedAccessException("Forkert kodeord! Prøv igen");

            return user;
        }

        public void Register(string username, string password)
        {
            throw new NotImplementedException();
        }
    }
}
