﻿using System;

namespace Backend
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create a testing user
            UserRepository userRepo = new UserRepository();
            Auth auth = new Auth(userRepo);

            User user = new User("Admin", "password");
            userRepo.Add(user);
            Console.WriteLine("Testbruger \"{0}\" blev tilføjet med adgangskode \"{1}\"!", user.Username, user.Password);

            Console.ReadLine();

            try
            {
                auth.Login("Admin", "password");
                Console.WriteLine("You've succesfully logged in!");
            }
            catch (UnauthorizedAccessException e)
            {
                Console.WriteLine(e);
            }
               
            Console.ReadLine();
        }
    }
}
