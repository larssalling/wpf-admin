﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Backend
{
    interface IRepository<Entity>
    {
        void Add(Entity entity);

        void Delete(Entity entity);
        void Update(Entity entity, Entity newEntity);
        Entity GetByID();
        IEnumerable<Entity> GetAll();
    }
}
