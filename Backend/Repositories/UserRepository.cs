﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Backend
{
    public class UserRepository : IRepository<User>
    {
        private List<User> users;

        public UserRepository()
        {
            this.users = new List<User>();
            User admin = new User("Admin", "password");
            this.Add(admin);
        }

        public void Add(User user)
        {
            this.users.Add(user);
        }

        public void Delete(User user)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetAll()
        {
            throw new NotImplementedException();
        }

        public User GetByID()
        {
            throw new NotImplementedException();
        }

        public User GetByUsername(string username)
        {
            return this.users.Find(x => x.Username == username);
        }

        public void Update(User user, User newUser)
        {
            throw new NotImplementedException();
        }
    }
}
